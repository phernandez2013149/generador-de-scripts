﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading.Tasks;
using CreadorDeScript.controladores;
using CreadorDeScript.modelos;
using System.Collections;
using System.Reflection;

namespace CreadorDeScript
{
    class Program
    {

        static private cargadorDeDatos datos;
        static private StreamWriter sw = new StreamWriter("C:\\Pablo\\script.sql");

        //concatena la cantidad de ceros
        static public string ceros(int distancia) 
        {
            string temp = "";
            for (int i = 1; i <= distancia; i++)
            {
                temp = temp + "0";
            }
            return temp;
        }


        static public void query(string p) {
           
            if (p.Equals("0") !=true)
            {
                sw.WriteLine("    C" + ceros(5 - p.Length) + p + " DECIMAL(5,2),");
            }
          
        }
        //principal
        static void Main(string[] args)
        {
            datos = new cargadorDeDatos();
            string n1, n2;
            foreach (VWKEYSXTPREC tem in datos.vwkeyArray)
            {
                n1 = (tem.VWKEYTPRECCOD.ToString());
                n2 = (tem.VWKEYCOD.ToString());
                n1 = ceros(5 - n1.Length) + n1;
                n2 = ceros(3 - n2.Length) + n2;

                Type mType = tem.GetType();
                PropertyInfo[] m_propiedades = mType.GetProperties();
                int contador= 0;

                sw.WriteLine("CREATE TABLE VWT" + n1 + n2 + "( ");

                foreach (PropertyInfo m_propiedad in m_propiedades) 
                {
                    contador++;
                    if(contador>3 & contador <45)
                    {
                        query(m_propiedad.GetValue(tem, null).ToString());
                    }
                }

                sw.WriteLine(");");
                sw.WriteLine(" ");

            }
            sw.Close();

        }

       
    }
}






